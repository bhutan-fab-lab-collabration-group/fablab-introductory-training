# FabLab introductory Training 

**Module discription**
 # Module 1: (Tshering Wangzom)
- [Whats a Fablab](http://fabfoundation.fablabbcn.org/index.php/about-fab-foundation/index.html)
- FabLab charter
- What's in a fablab
- Fablabs in Bhutan
- Fab Network
- Potentials
- Super Fablab
- Way Forward
- Collaboration among the Fablabs 

# Module 2 (Jigme Tharchen):
- Gitlab
- Git Bash
- Push and pull into repository	
- Project repository	
- Project sharing
- Version Management
- Project tracking

# Module 3: (Tshering Wangzom and Zina Yonten)
Design softwares:
- 2D design 
- 3D design
- TinkerCad
- Design using Fusion
- Learning Path



# Module 4: (Zina Yonten)
- 3 D printing
- What are 3D printers
- Types of 3D printers
- Types of filament
- Applications and tech specs
- Softwares
- Design and print



# Module 5: (Pema Thinley and Manish Rai)
- Electronics Design and production
- PCB
- Softwares
- Designing a PCB
- Using the right components for the right application	

	
# Module 6 (Manish Rai):
- Laser Cutter
- Design
- Machines
- Do’s and Dont’s
- Applications


# Module 7 (Jigme Tharchen)
- Shop Bot
- Design
- Machines
- Do’s and Dont’s
- Applications


# Module 8:(Zina Yonten)
- Design Thinking for digital fabrication
- From ideas to prototyping
- Consumer centric products
- Integration of FABTechnologies with your projects

	
# Module 9 (Jigme Tharchen):
- Application and software programming
- ArduinoIDE
- Program a uC
- Designing algorithms and flow charts for easy implementation
- Frameworks and programming languages


# Module 10 (Pema Thinley & Tshering Wangzom):
- Inventory management
- Maintenance of machines
- SOPs
- Do’s and Dont’s


